const fs = require("fs");
var strLinks = '';

fs.readdirSync("../../images/accessories/necklaces/").forEach(file => {
    //Print file name
    var filename = file.replace('.webp','')
    strLinks += '<a href="'+filename+'.html"><img class="centered-and-cropped" src="../../images/accessories/necklaces/'+file+'" alt="" /></a>';

    /*
    Run this to print the file contents
    console.log(readFileSync(".levels/" + file, {encoding: "utf8"}))
    */
})
document.getElementById("links").innerHTML = strLinks