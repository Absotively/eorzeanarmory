﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance Force
; #Warn  ; Enable warnings to assist with detecting common errors.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
CoordMode, Mouse, Screen
CoordMode, Pixel, Screen

global itemToShoot := "Necklace"
global buttonX := 960
global buttonY := 540
global centerX := 960
global centerY := 540
global stepInterval := 250
global stepPad := 50
global keyHoldInterval := 75
global keyHoldPad := 25

Switch itemToShoot
{
	Case "Ring":
		buttonX = 295
		buttonY = 689
	Case "Bracelet":
		buttonX = 295
		buttonY = 635
	Case "Necklace":
		buttonX = 295
		buttonY = 585
	Case "Earring":
		buttonX = 295
		buttonY = 537
}

global maxLoops :=2
global counter := 0
ID := WinExist("A")

InputBox, maxLoops, Number of Loops, How many times should we loop?, false, 250, 125
if ErrorLevel
	ExitApp

; Click, arrow down, close. Move mouse, scroll down, click, 
; arrow *up*, arrow down, close, hide ui, screenshot, show ui

While (counter < maxLoops) {
	; Click
	Sleep, Pad(stepInterval, stepPad)
	MouseMove, buttonX, buttonY, 5
	Click, Down
	Sleep, Pad(keyHoldInterval, keyHoldPad)
	Click, Up

	; Select next item
	Sleep, Pad(stepInterval, stepPad)
	Send {Down down}
	Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {Down up}

	; Close dropdown
	Sleep, Pad(stepInterval, stepPad)
	Send {Esc down}
	Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {Esc up}

;	; Move mouse
;	Sleep, Pad(stepInterval, stepPad)
;	MouseMove, centerX, centerY, 5
;
;	; Scroll down
;	Sleep, Pad(stepInterval, stepPad)
;	Send {WheelDown 1}
;
;	; Click
;	Sleep, Pad(stepInterval, stepPad)
;	MouseMove, buttonX, buttonY, 5
;	Click, Down
;	Sleep, Pad(keyHoldInterval, keyHoldPad)
;	Click, Up
;
;	; Select next item
;	Sleep, Pad(stepInterval, stepPad)
;	Send {Up down}
;	Sleep, Pad(keyHoldInterval, keyHoldPad)
;	Send {Up up}
;	Sleep, Pad(stepInterval, stepPad)
;	Send {Down down}
;	Sleep, Pad(keyHoldInterval, keyHoldPad)
;	Send {Down up}
;
;	; Close dropdown
;	Sleep, Pad(stepInterval, stepPad)
;	Send {Esc down}
;	Sleep, Pad(keyHoldInterval, keyHoldPad)
;	Send {Esc up}
;
;	; Move mouse
;	Sleep, Pad(stepInterval, stepPad)
;	MouseMove, centerX, centerY, 5
;
;	; Scroll down
;	Sleep, Pad(stepInterval, stepPad)
;	Send {WheelDown 1}
;
	; Hide UI
	Sleep, Pad(stepInterval, stepPad)
	Send {shift down}
	; Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {x down}
	Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {x up}
	; Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {shift up}

	; Take screenshot
	Sleep, Pad(stepInterval, stepPad)
	Send {shift down}
	; Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {NumpadAdd down}
	Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {NumpadAdd up}
	; Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {shift up}
	;Send {PrintScreen}

	; Show UI
	Sleep, Pad(stepInterval, stepPad)
	Send {shift down}
	; Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {x down}
	Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {x up}
	; Sleep, Pad(keyHoldInterval, keyHoldPad)
	Send {shift up}
	; Wait to repeat
	Sleep, Pad(stepInterval, stepPad)
	; Increment counter
	counter := counter + 1
}


Pad(initialNumber, padRange)
{
	Random, padAmount, 0-padRange, padRange
	if (padAmount = -50)
		padAmount = -25

	paddedNumber := initialNumber + padAmount
	;MsgBox, %paddedCoordinate% ; Debug
	return paddedNumber
}

+Esc::ExitApp