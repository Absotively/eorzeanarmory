# Get all files in directory
# Check filename against list
# Rename file to model name
import os
import csv
import pandas

workingpath = 'D:\Dropbox\Tech\Websites\eorzean-armory\public\images\\accessories\\earrings\\right'
datafile = 'D:\Dropbox\Tech\Websites\eorzean-armory\Item Lists\Filenames\\Earrings - Right.txt'

with open(datafile) as openDataFile:
	reader = csv.reader(openDataFile, delimiter="\t")
	filenames = dict(reader)

dir_list = os.listdir(workingpath)
for file in dir_list:
	if(file in filenames):
		# rename file
		# print(str(filenames[file]))
		os.rename(workingpath + "/" + file, workingpath + "/" + str(filenames[file]))